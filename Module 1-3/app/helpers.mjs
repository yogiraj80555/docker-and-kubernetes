const connectToDatabase = () => {
    const dummyPromise = new Promise ((resolve, reject) => {
        setTimeout (() => {
            resolve();

        }, 3000);
    });
    return dummyPromise
};

export default connectToDatabase;
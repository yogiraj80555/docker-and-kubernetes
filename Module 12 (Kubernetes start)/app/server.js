const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.get('/', (req, res) => {
  res.send(`
    <h1>Hello from this NodeJs app! </h1>
    <p> Try sending a request to /error. </p>
    <p> This will get always Pull with tag <b>imagePullPolicy: <i>always </i></b> tag. </p>
  `);
});

app.get('/exists', (req, res) => {
 process.exit(1);
});

app.listen(8080);
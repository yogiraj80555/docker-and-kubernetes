FROM nginx:stable-alpine

WORKDIR /etc/nginx/conf.d/

COPY nginx/nginx.conf .

# Renaming nginx.conf file
RUN mv nginx.conf default.conf  

WORKDIR /var/www/html

# Copying src dir contents to /var/www/html 
COPY src .


#the above image have default run command which start the server so we not write any run command here

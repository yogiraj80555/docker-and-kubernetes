# we need php-fpm image for Nginx
FROM php:8.2.4-fpm-alpine

# standard folder on web server to serve website from. and this will hold our laravel PHP application. And I think Nginx uses this folder to see php pages
WORKDIR /var/www/html

COPY src .


# PHP extensions
RUN docker-php-ext-install pdo pdo_mysql

# The php image or any image do not give us to write permission inside the container when they are not bind with volumes there for we need to give then read-write permission like below.
# "www-data" is default user created by php image 
RUN chown -R www-data:www-data /var/www/html

# Here PHP base image have a default command which is executed. Which invoked PHP interpreter. So this image which we're building here will then automatically run this default command of the base image.
# And therefore it will be able to deal with incoming PHP files that should be interpreted because our base image is invoking this interpreter.

# Note: If you don't have a command or entry point at the end, then the command or entry point of the base image will be used if it has any. 
# And here in our base image i.e 'php:7.4-fpm-alpine' has one default command which invokes the PHP interpreter.



